1996��10�� ��������

1. I want to play tennis tomorrow but I didn't bring my racket with me this evening.
Do you have one I could borrow? 
What does the man imply?

2. I thought this shirt was a great deal but I washed it once and it shrunk so much that I can't wear it.
Some bargain. You should ask for a refund.
What does the woman mean?

3. I broke my ankle last Tuesday. And now I have to be on crutches for six weeks.
I'm sorry to hear that. Is there anything I can do for you?
What does the woman mean?

4. Why didn't you call me last night like you were supposed to?
I did your time was busy.
What does the man mean?

5. Sue, would you like a sandwich or something? 
Oh, please don't bother. I can get something later.
What does the woman mean?

6. This report is due tomorrow. Would you be able to work on it with me tonight?
Unfortunately I have another commitment.
What does the woman mean?

7. I wonder where the books I ordered are. I expected to receive the package several days ago.
Maybe you'd better check it with the company. They could be temporarily out of stock.
What does the man suggest the woman do?

8. I just have to type this last page and then I'm through.
By then I'll be done too.
What does the woman mean?

9. Did you read the editorial in the paper about the mayor's speech?
I sure did. But I think they twisted the meaning of what he said.
What does the man say of the editorial?

10. Why do we go to see a movie tonight? A good comedy might cheer you up.
I would. But I the reason that I've been so down is all this work I have to do.
What can be inferred about the man?

11. I'd like to try to sell some of my textbooks from the last semester.
   You and a few hundred other people.
   What does the woman imply?

12. Could I talk to you for a minute about the discrepancy I found in this graph?
I'm kind of in the middle of things right now.
   What does the woman mean?

13. Do you know if George is coming to the meeting?
Oh, no. I was supposed to tell that he is sick and can't come.
   What does the man mean?

14. Dick, please don't tie up the phone. I need to make a call. 
I'll be off in a minute.
What will the man do?

15. Saliy says we should meet her in the park at noon.
I thought we were meeting at the library.
What do the speakers disagree about?

16. I'm sort of upset with my brother. He hasn't answer either of my letters.
Well, just remember how hectic your freshman year was.
Give him a chance to get settled.
What does the man imply?

17. I wonder what this new flavor of ice cream tastes like?
I tried it last week. If I were you, I would stick with an old favorite. 
What can be inferred from the conversation?

18. Pete had hoped to have his apartment painted by this time.
But he hasn't started yet, has he?
What does the woman imply about Pete?

19. You don't believe in diet, do you?
There is nothing wrong with them but they have to combine with exercise to do any good.
What does the man mean?

20. I'm amazed that you still haven't gotten to know your neighbors.
They tend to keep to themselves.
What does the man mean?

21. Joe, could you please help me straighten the rug? I'll move it while you lift up this side of the desk.
Sure. Oh, just a minute. The lamp is still on it.
What will the man probably do next?
22. I had to stand in line at the bank for about half an hour. There were only two tellers open.
That's where you and I differ. I would've gone back another time.
What does the woman imply?

23. I was touched that our neighbor brought over a dish when we moved in.
Yeah. Mrs. Smith really goes out of her way for others.
What can be inferred about Mrs. Smith?

24. Where are you up to now?
To meet a friend to come in on the subway.
What does the woman mean?

25. My company is flying me out to Hawaii on business next week.
That's great. Where are they putting you up?
What does the man want to know?

26. It's good to see you, Mary. How have you been?
Actually I have been feeling under the weather recently. 
What does the woman mean?

27. It's a really nice apartment. But the owners want two-month rent in advance and I just don't have it.
Do you think it would help if they knew what a good tenant you are? You could get your landlord to write them a letter.
What does the woman suggest the man do?

28. Good morning, East Coast Data Process. May I help you?
Caroline? Oh, dear, I'm sorry, I thought I dialed Jack Easton, your number must be just above his in my address book.
   What can be inferred from the conversation?

29. There's a nonstop train for Washington leaving here at 2:15. 
That will be faster than taking the one that leaves at 2:00 and it will give us time to get a bite to eat.
What does the woman imply?

30. Sorry about your report. I didn't realize it was in that stack of papers.
Don't worry about it. Luckily I saved a copy on my computer.
What can be inferred from the conversation?

PART B
31-34 Conversation between two students.
* Hi, Bob. How is your oral presentation coming along?
* What oral presentation? I don't have to give mine until the end of the next week.
* You mean you haven't even started working on it yet?
* No. I need the pressure of a deadline to get inspired.
* Gee. I'm just the opposite. I can't concentrate unless I know I have plenty of time. Besides I have a big physics test next week so I want to get my presentation out of the way.
* What's your presentation on?
* William Carlos Williams.
* Wasn't he a poet? I thought we were supposed to focus on a short story.
* He wrote short stories, do you know?
* Really? I never knew that I guess I'll learn more on Tuesday. Is that when you are supposed to talk?
* Uh-huh. But I was going to offer you sneak preview. I'm looking for someone to tell me whether I'm talking to fast and whether you can follow what I'm saying.
* I guess I could do that. When were you thinking of?
* Will tonight be all right? I have a last to go to now but I'll be around after dinner.
* That sounds good. I'll stop by your room. It's probably quieter there than in my hall. 

31. What do the students mainly discuss?
32. What does the woman want the man to tell her?
33. What will the man probably do after dinner?
34. Where do the students arrange the meet?

35-37 Conversation between two students. 
* Have you ever read anything about pseudo sciences?
* You mean fake sciences? Yes. In fact I was just reading some articles about the brain. I have been looking through some of my roommate's science magazines and I came across an article on phrenology.
* Phrenology, wasn't that the pseudo science founded by the scientist Franz Gall?
* Yes. Gall maintained that people's characters could be determined by the size and the shape of their skulls. For example he though that a pump in a certain place on the head means that the person had the ability of a musician.
* Really? I wonder what phrenologists would say about the bumps on my head. Would they say I have the abilities to be a doctor, or a plumber, or a thief.
* Well, I'm not sure exactly what the connection is between a person's abilities and the physical characteristics of the head. But although there's no scientific basis for phrenology, it is true that the bead is the center control for the rest of the body.
* I guess you are right. Scientists now know that different parts of the brain control different parts of the body.
* Yes. And I wouldn't be surprised that the scientists one day discover that certain aspect of phrenology has scientific application.

35. What is the source of the woman's information?
36. According to phrenology, what determines a person's character?
37. What does the woman say about the sections of the brain?
 
PART C
38 to 42 Talk given in a library science class.
In the early 1800s, the paper industry was still using rags as its basic source of fiber as it had for many centuries. However the rag supply couldn't keep up with the growing demand for paper. The United States alone was using 250 thousand tons of rags each year. And a quarter of that had to be imported. It was clear that a new source of fiber was needed to keep up with the demand for paper. The answer to this problem turned out to be paper made from wood pulp, something that was abundantly available in north America. IN Canada, the first wood pulp mill was set up in 1866 and it was immediately successful. But while wood pulp solved the problem of quantity it created a problem of quality. Wood contains a substance called lignin. The simplest way to make large quantities of cheap paper involved leaving the lignin in the wood pulp. But lignin is acidic and its presence in paper has shorten the life expectancy of paper from several centuries for rag pager to less than a century for paper made from wood pulp. This means that books printed less than a hundred years ago are already turning yellow and beginning to disintegrate, even though books printed much earlier maybe in the conditions. This is bad enough for the older books on your bookshelf but it opposes a huge problem for libraries and the collections of government documents.

38. What does the speaker mainly discuss?  
39. What did the paper industry need a new source of fiber in the early 1800s?
40. What can inferred about the first wood pulp mill in Canada?
41. According to the speaker, what is the problem with lignin?
42. According to the speaker, what problem do libraries face?

43 to 47 Radio announcer talking about a current topic.
A recent report has shown that here in the United States, we've experienced an evolution concerning our attitudes towards the workweek and the weekends. Although some calendars still mark the beginning of a week as Sunday, more and more of us are coming to regard Monday as the first day of the week with Saturday and Sunday comprising the two-day period thought as the weekend. In fact the word "weekend" didn't even exist in English until about the middle of last century. In England at that time, Saturday afternoons had just been added to Sundays and holidays as a time for workers to have off from their jobs. This innovation became common in the United States in the 1920s, but as the workweek were shortened during the Great Depression of the 1930s, the weekend expanded to two full days - Saturday and Sunday. Some people thought that this trend would continue due to increasing automation and the workweek might decrease to four days or even fewer. But so far this hasn't happened. The workweek seems to have stabilized as forty hours made up of five eight-hour days. After this commercial I'll be back to talk about the idea of adding Monday to the weekend.

43. What is the speaker mainly discussing?
44. According to the speaker, what is changing in the way people think about the week?
45. According to the speaker, how has the amount of time people work changed from the early part of the century?
46. What does the speaker imply about the workweek in England in early 1800s?
47. According to the speaker, what affect did some people think the increasing use of automation would have?

48 to 50 A professor talking to her music students.
I don't think I have told you about my trip to Tanglewood's music festival. When I was in college, I won a music competition and the prize was a week at Tanglewood. Anyway it is one of the world's most famous music festivals and the summer pome the Boston Symphony Orchestra. It is located in the beautiful Berkshire Hills in New England. The summer musical season consists given over about mine weeks: from just 1st through the first week in September. The biggest  stars on the music scene appear here. The year I went I was Lucky enough to see Leonard Bemstein conducting. I understand it is sometimes hard to get tickets but of course mine were a part of the prize. If you want to sit inside the tickets are expensive. It's much cheaper to sit outside on the lawn. But it might rain, or sometimes it is really cool even in the summer. Either way the sound system is excellent. So it doesn't really matter where you sit. I seem to recall that the festival got started in the 1930s. Some Berkshire residents invited a symphony orchestra to perform a few outdoor concerts. The concerts were so successful that after a couple of years that things really took off. And the festival has got bigger and better every year. Attending was such a wonderful experience. I'd love to be able to go again. And I hope that all of you would be able to go too.

48. Why did the professor originally go to Tanglewood?
49. According to the professor, what is the disadvantage of sitting on the lawn?
50. What does the professor imply about the festival?
51. What is the killdeer's nest in parking lot an example of?  (A)
52. According to the speaker, what is a possible reason that birds began to build nests in trees?  (A)

