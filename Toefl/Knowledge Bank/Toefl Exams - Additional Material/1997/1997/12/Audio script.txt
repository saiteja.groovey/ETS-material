1997年12月NA 听力文字

Section 1. Listening Comprehension
1. (B)
W：Have you been to the new gym since it opened?
M: Are you kidding? Tomorrow is the deadline for my project.
Q: What does the man imply? 
2. (B)
M: I’d like to borrow that book after you’ve finished it.
W: Sure. But I promised it to Jane first.
Q: What does the woman intend to do? 
3. (A)
W: I could really use more room. My apartment is so small and there’s no closet (壁橱) space.
M: You should see the apartments in my building. You know, one of my neighbors is moving out. Come by and I’ll bring you to his place.
Q: What does the man imply? 
4. (A)
M: The forecast calls for (预报说有) heavy snow again tonight. Aren’t you glad we’ll be getting away from this for a week?
W: I sure am. But let’s call tomorrow morning before we leave for the airport to make sure our flight hasn’t been delayed or canceled.
Q: What does the woman suggest they do? 
5. (D)
M: Has anyone turned in a brown leather wallet? Mine seems to be lost, and it has my driver’s license in it, and also some family pictures that are pretty important to me.
M: Oh, I think one like that was brought in this morning. Wait here just a minute, please.
Q: What will the woman probably do? 
6. (B)
M: I’d like you to come with me to the opening of the photography exhibit.
W: I’m exhausted. You’ll have to manage without me tonight.
Q: What will the woman probably do? 
7. (B)
W: Guess what I just heard. Dave’s selling that car of his that you like so much?
M: Oh, wow. I’ll bet it’s expensive, but it couldn’t hurt to check it out.
Q: What will the man probably do? 
8. (B)
W: I have an idea for a special issue of the school newspaper. Do you have time to discuss it?
M: My class is over at one, but I’m free after that.
Q: What does the man mean? 
9. (C)
W: Did you return that book to the library for me? I don’t want to pay a fine.
M: Don’t worry about it. I took care of it.
Q: What does the man mean? 
10. (B)
M: I’m really sorry I missed the pop art exhibit at the museum.
W: You might try to catch it when it opens in New York next month.
Q: What does the woman suggest that the man do? 
11. (A)
M: Wasn’t there once a bakery (面包店) here?
W: Yes. But it went out of business (停业) last year.
Q: What does the woman mean? 
12. (D)
M: Oh, I’m so sorry. You must let me pay to have your jacket cleaned.
W: That’s all right. It could happen to anyone. And I’m sure that orange juice doesn’t stain (染污).
Q: What can be inferred about the woman? 
13. (C)
W: What are your new blue jeans like?
M: Oh, they are pretty much like the other ones except for the larger waist. I guess I don’t have much time to exercise these days.
Q: What does the woman mean? 
14. (D)
W1: If you’re trying to fix this bookcase in here. You’ll have to turn your desk sideways (向侧面).
W2: I guess you are right. But I hate to lose the view I have from my window.
Q: What are the women doing? 
15. (B)
W: Did you hear there are some new kinds of cable television system that will allow you to get five hundred channels?
M: Yeah. But I have a hunch (预感) we’ll have nothing to watch that is different from what we have now.
Q: What can be inferred from the man’s reaction to the new television system? 
16. (A)
W: I hope you remembered to pick up my clothes from the cleaner’s.
M: I couldn’t go because the car wouldn’t start.
Q: What does the man mean? 
17. (B)
W1: I must have told Mike five times not to forget the meeting. And he still missed it.
W2: Well, you know Mike, everything’s in one ear and out the other.
Q: What can be inferred about Mike? 
18. (C)
M: Have you seen John since he started wearing contact lenses (隐形眼镜)?
W: I almost didn’t recognize him at first.
Q: What does the woman mean? 
19. (D)
W: I still don’t feel well. I don’t know what I’m going to do.
M: I think the health center’s open late tonight.
Q: What does the man imply the woman should do? 
20. (A)
W: Say, Richard. If you like antique cars (老爷车), we’ve got an extra ticket for the auto show on Saturday. Care to join us?
M: Gee. How could I turn down an offer like that?
Q: What does the man mean? 
21. (D)
W: That new soap I’ve been using lately smells nice, but it dries my skin out.
M: It’s probably all those harsh (刺激性的) chemicals. You should try the kind I use. It’s all natural.
Q: What does the man suggest the woman do? 
22. (B)
M: That bread I bought yesterday isn’t in the kitchen. Someone must’ve eaten it.
W: Look on top of the refrigerator.
Q: What does the woman imply? 
23. (B)
M: Can you believe this great gift Sharon sent you?
W: I know. She really has a heart of gold.
Q: What can be inferred about Sharon? 
24. (D)
W: I heard you auditioned (试唱) for the chorus (合唱团). How did it go?
M: Oh, well. The director is pretty high standards. I guess I just didn’t measure up (达到标准).
Q: What can be inferred about the man? 
25. (D)
W: The weather is certainly unusual for this time of year.
M: Yeah. So warm and humid.
Q: What does the man imply? 
26. (B)
W: Basketball practice doesn’t take a lot of time, does it?
M: Only every spare minute.
Q: What does the man imply about basketball practice? 
27. (B)
W: What are you doing here? You aren’t in the film class.
M: I changed my schedule. Movies are a good change of pace from all these chemistry experiments.
Q: What does the man mean? 
28. (C)
W: Waiting in line to copy just one page of an article wastes so much time.
M: Have you ever tried the photocopier on the third floor of the library? I don’t think as many people know about it.
Q: What does the man suggest that the woman do? 
29. (B)
W: With all of these typos in this résumé, you are not going to make a very good impression.
M: Good thing it’s on the word processor (文字处理软件).
Q: What will the man probably do? 
30. (A)
M: I have two exams and three papers to get done in the next couple of days.
W: How did it get so backed up (堆积)?
Q: What does the woman imply about the man? 

Questions 31-33. Listen to a radio interview with the artistic director of a dance company.
M: Today’s arts report features (特邀某人主讲) Dan Parker of the American India Dance Theater. Mr. Parker, I understand your troupe performs traditional music and dance from many different Native American cultures. Can you give us an idea of some of the dances you’ll be doing in your performance tonight?
W: Certainly. We’ll be doing one that’s a war dance. Originally it was a story telling device (方式) to recount (复述) battles. Another is the grass dance performed by the plains Indians, where they actually flatten tall field grass to prepare it for a ceremony.
M: Since your dancers are from many different tribes, how can you be sure the dances are done correctly?
W: Everything we do has been approved by the elders of our tribes. That’s partly because we don’t necessarily know each other’s styles or dances. But it’s also because it’s hard to get complete agreement, even within the same tribe, about exactly how the dance should be done.
M: Anyone who attends one of your performances will notice that your company goes to a lot of trouble to provide detailed explanations of the origin of the dances, the music, the costumes and so forth. Could you explain to our listeners why you do this?
W: Good question. There are always concerns that traditional dances performed in a theater are nothing more than a spectacle. Our explanations show that in our cultures, dance is ritual rather than entertainment. We also want to make it clear to our audience that we are not performing any dances used for sacred ceremonies.
31. (A) What’s the main topic of the conversation? 
32. (D) What’s the purpose of the interview? 
33. (B) Why are the dances approved by the elders of the tribes? 

Question 34-37. Listen to a phone conversation between two friends who are discussing a problem.
M: Hello.
W: Hello, Sam. This is Paula Handson. Sorry to brother you. But I’m having a small problem I thought you might be able to help me with.
W: Sure, Paula. What’s up?
M: Well, you know Sarah and I moved into an off-campus apartment in the fall, over on the west side of town? Anyway, we’ve been happy with it until the past couple of months.
W: Yeah. What happened?
M: Well, the dishwasher broke down. So we reported it to Ms Connors, the owner. She 
said she’d take care of it. But a month went by and nothing happened.
W: Did you get back in touch with her?
M: I got a repairperson to give me an estimate (报价), then I sent it to her. When I didn’t hear from her, I had the repair done. And I deducted the cost from the rent check.
W: So what’s the problem?
M: She called here mad as a hornet (大黄蜂). She said she could have gotten the repair done for less money. Now she’s threatening to evict (撵) us for not paying the full rent.
W: Hold on, Paula. It does sound pretty serious. But I’m sure you can all sit down and work this out.
M: Well, you are over at the law school. So I wondered if you would mind coming with Sarah and me when we go to talk to Ms. Connors. We’re supposed to meet with her tomorrow night at eight.
W: Sure. I haven’t studied a lot about contracts yet. But I’d be glad to help you straighten things out. Why don’t I stop by at about 7:30?
M: Thanks, Sam. You’re a lifesaver.
34. (C) Why is Paula unhappy? 
35. (D) Why is Ms. Connors angry? 
36. (B) What are Paula and her roommate planning to do? 
37. (A) Why does Paula think Sam can help her? 

Questions 38-42. Listen to part of a lecture in speech class.
Today we are going to practice evaluating the main tool used when addressing groups—the voice. There are three main elements that combine to create either a positive or negative experience for listeners. They can result in a voice that is pleasing to listen to and can be used effectively, or even worse, causes an adversary (敌对的) reaction. The three elements are volume, pitch (音调), and pace. When evaluating volume, keep in mind that a good speaker will adjust to the size of both the room and the audience. Of course, with an amplifying device like a microphone, the speaker can use a natural tone. But speaker should not be dependent on microphones. A good speaker can speak loudly without shouting. The second element—pitch—is related to the highness and lowness of the sounds. High pitches are for most people more difficult to listen to, so in general speaker should use the lower registers (声域) of the voice. During a presentation, it’s important to vary pitch to some extent in order to maintain interest. The third element, pace, that is how fast or slow words and sounds are articulated (发出), should also be varied. A slower pace can be used to emphasize important points. Note that the time spent not speaking can be meaningful, too. Pauses ought to be used to signal transitions (转折) or create anticipation (期待). Because a pause gives the listener time to think about what was just said, or even to predict what might come next, it can be very effective when moving from one topic to another. What I’d like you to do now is watch and listen to a video tape and use the forms I gave you to rate (评价) the speaking voices you hear. Then tonight I want you to go home and read a passage into a tape-recorder and evaluate your own voice.
38. (B) What is the main point the professor makes? 
39. (C) According to the professor, what can a speaker do to keep an audience’s attention? 
40. (D) What recommendation does the professor make about volume? 
41. (A) According to the professor, how can a speaker indicate that the topic is about to change? 
42. (D) What are the students going to use a tape-recorder for? 

Questions 43-46. Listen to part of a lecture at a museum.
Let’s proceed to the main exhibit hall and look at some of the actual vehicles that played a prominent (突出的) role in speeding up mail delivery. Consider how long it used to take to send a letter across a relatively short distance. Back in the 1600s it took two weeks on horseback to get a letter from Boston to New York, a distance of about 260 miles. Crossing a river was also a challenge. Ferry (轮渡) service was so irregular that a carrier would sometimes wait hours just to catch a ferry. For journeys inland there was always a stagecoach (驿站马车), but the ride was by no means comfortable because it had to be shared with other passengers. The post office was pretty ingenious (有独创性) about animals. In the 19th century, in the southwestern desert, for instance, camels were brought in to help to get the mail through. In Alaska, reindeer were used. This practice was discontinued because of the disagreeable temperament (脾性) of these animals. We’ll stop here a minute so that you can enter this replica (复制品) of a railway mail car. It was during the age of the iron horse that delivery really started to pick up. In fact the United States transported most bulk (大宗) mail by train for nearly 100 years. The first airmail service didn’t start until 1918. Please take a few moments to look around. I hope you’ll enjoy your tour. And as you continue on your own, may I suggest you visit our impressive philatelic (集邮的) collection? Not only can you look at some of the more unusual stamps issued but there’s an interesting exhibit on how stamps are made.
43. (B) What is the talk mainly about? 
44. (D) According to the speaker, why was it a problem for mail carriers to cross rivers in 1600s? 
45. (A) What does the expression the “age of the iron horse” refer to? 
46. (C) What can be found in the museum’s philatelic collection? 

Questions 47-50. Listen to the beginning of a talk on astronomy.
Most people think of astronomers as people who spend their time in cold observatories (观察站) peering (窥视) through telescopes every night. In fact a typical astronomer spends most of his/her time analyzing data and may only be at the telescope a few weeks of the year. Some astronomers work on purely theoretical problems and never use a telescope at all. You might not know how rarely images are viewed directly through telescopes. The most common way to observe the sky is to photograph them. The process is very simple. First a photographic plate (板) is coated with (涂上) a light-sensitive material. The plate is positioned so that the image received by the telescope is recorded on it. Then the image can be developed, enlarged and published so that many people can study it. Because most astronomical objects are very remote, the light we receive from them is rather feeble (weak). But by using a telescope as a camera, long time exposures can be made. In this way objects can be photographed that are a hundred times too faint to be seen by just looking through a telescope.
