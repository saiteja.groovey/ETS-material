1999��1�� ��������

1. Why are you leaving so early? The movie doesn't start till seven.
I don't want to be at the traffic there. It's a nightmare on the express way during rush hour.
What does the man mean?  (A)

2. Excuse me, but could you tell me how to get to the Excelsior Hotel? I thought it was on this corner but I seem to have made a mistake.
Hmm. I'm sorry. Maybe you should try calling them. There is a phone over there by the candy store.
What does the woman suggest the man do?  (C)

3. Can I borrow your calculus textbook? I left mine in the classroom. And it was gone when I went back.
That happened to me once. I'd almost given up on finding it until I checked it at the lost-and-found at the information desk downstairs in the lobby.
What does the woman imply about the man should do?  (D)

4. Did you see the diamond ring Bill gave to Linda?
I sure did. It must have cost him an arm and a leg.
What does the woman imply about the ring  (C)

5. I'm always late for my morning classes. It's because of all the traffic out near where I live.
Well, you wouldn't have that problem if you move into our campus.
What does the man suggest the woman do?  (D)

6. Jennifer is going to the shore again this weekend.
Well, she's always been a beach person.
What can be inferred about Jennifer.  (A)

7. Hey, Larry. Wanna meet a few of us for coffee in a little while?
Hmm. I would if I weren't so far behind in this reading I'm doing for history.
What will the man probably do?  (A)

8. Oh, something in this room is making my eyes edge. I must be allergic to something.
Hmm. I wonder what it is.
What does the woman want to know?  (C)

9. My brother is coming this weekend and I thought three of us could go out to dinner Saturday night. Any suggestions?
It's up to you. I don't know the restaurants around here that well. So you know a better place to go than me. 
What does the man mean?  (B)

10. Can I get a ride into the office with you tomorrow?
Another day would be fine. But I got to be downtown for a meeting first thing in the morning.
What does the woman imply?   (D)

11. After two weeks of tennis lessons I think I finally managed to improve my backhand. 
Like my mom always says: "Practice makes perfect." 
What does the woman mean?  (B)

12. I just heard about your acceptance into law school. Do you think you will be able to join your brother's firm when you graduate?
Not likely. He is a tax lawyer and I'm going to major in criminal law.
What does the woman imply?  (D)

13. Could you give me a ride to the dean's office? My interview for the scholarship is in an hour.
Don't you have anything else to wear other than that sweater?
What does the woman imply?  (D)

14. Well, if you are seriously considering buying a car, I'm trying to get rid of mine. All it needs is some new paint.
Thanks. But most used cars end up being more trouble than they are worth.
What will the man probably do?  (C)

15. Did you watch that comedy special on TV last night? I don't think I ever laughed so hard.
Don't even talk to me about it. The only night I really wanted to watch something and we had a power fail in my building.
What does the woman mean?  (A)

16. Do you think it'll be able to get this ink stain out of my pants?
It won't be a problem but I'll need to send them over to the main cleaning facility. That's an extra day's time.
What does the woman mean?  (A)

17. You'll have to be a lot more persuasive if you want to convince the committee to accept your proposal.
I'm not sure what else I could say. Besides I don't think they will reject it.
What does the woman mean?  (C)

18. I'm having trouble slicing the bread with this knife.
Oh. Sorry about that. I haven't gotten around to sharpening it yet.
What can be inferred about the knife?  (B)

19. Do you know if Sarah has reserved the room for the committee meeting yet?
No. But if she hasn't we should have her try to get it at the auditorium. 
We'll need the space.
What does the woman want Sarah to do?  (C)

20. I lost the piece of paper Laura gave me. You know, the one with her address on it.
You might be able to find it listed in the phone book/
What does the woman mean?  (D)

21. I heard on the radio that not only is it going to be super hot tomorrow but also the humidity's going way up.
Sounds like I'm going to have to find an air-conditioned place to be in. 
What does the man mean?  (B)

22. I can't concentrate on this final report any longer. Maybe I should take a nap before we continue.
You know they say the physical activity makes you more alert.
What does the woman imply?  (B)

23. I hope you like the novel I lent you. I wasn't sure whether it was the kind of book you would be interested in.
You know, I had the same doubt at first. But once I started I simply couldn't put it down.
What does the man mean?  (C)

24. The museum exhibit that our professor recommended just closed. Last day yesterday.
Oh. I was really looking forward to seeing it.
What does the man mean?  (C)

25. If George misses one more meeting we are going to have to find one new committee secretary.
We'd better give him a n ultimatum.
What does the man suggest they do?  (B)

26. Are you sure you don't mind getting the concert tickets? I wouldn't be able to pay you back until Friday when I get paid.
No problem. I'm glad I can help and we'll be able to go together.
What does the man mean?  (C)

27. Have heard the news? The manager posted this month's work schedule.
She did? Where?
What does the woman ask?  (C)

28. Will you be living in the dormitory this year?
Not if I can help it. I've been thinking of renting an apartment off campus with some friends of mine.
What does the man imply?  (D)

29. The plot of that movie is hard to find.
It makes more sense the second time.
What can be inferred about the woman?  (B)

30. I'm so soaked from the rain. I'd go back to my room to change my clothes if there were ore time before the performance.
I could use drying off too. But I hate the idea of missing even a few minutes of this concert.
What can be inferred about the speakers?  (D)

PART B   CONVERSATIONS
31-34 PART OF A TELEPHONE INTERVIEW
*Dr. Thomas? This is Keet Bradley from the daily news. I'd like to ask you some questions about the new official standard weight that you purchased. 
* I'd be happy to help you. What would you like to know?
* First of all, how was the standard weight used?
* Well, the people in our department use it to check the scales all over the country. The department of weights and measures, we are a government agency. It's our responsibility to see that all the scales measure a kilogram accurately so this is the way we use to adjust the scales.
* How did you check the scales before?
* We have an old standard weight that we used to use. It had to be replaced because it was imprecise. You see it was made of poor quality metal that was too porous. It absorbed too much moisture.
* Oh. So when the weather was humid it weighed more and when it was dry it weighed less.
* Exactly. And that variation can affect the standards of the whole country. So our department had the new weight made out of higher quality metal.
* How much did it cost.?
* About 45 thousand dollars. 
* 45,000 dollars? For a one kilogram weight? That's more expensive than gold. Is it really worth that much?
* I'm sure it is. Industries depend on our government agency to monitor the accuracy of scales so that when they buy and sell their products there is one standard. Think of the drug industry, for example, those companies rely on high accuracy scales to manufacture and package medicine.
31. What is the conversation mainly about?  (B)
32. How was the weight used?  (A)
33. Why was it necessary to replace the old standard weight?  (C)
34. What does Dr. Thomas probably think about the cost of the new weight?  (C)

35-38 CONVERSATION BETWEEN A STUDENT AND A PROFESSOR
* Elizabeth Martin speaking.
* Dr. Martin, my name is Mark Johnson. My roommate, Benjamin Jones, is in your art history class. Uh-m, Art History 502?
* Yes.
* Well, he is sick and won't be in your class today. He asked me to bring his term paper to your office.
* OK. The paper is due by 3 o'clock.
* I have a class from 1 to 2. I'll bring it to your office after my class.
* Well, I have a meeting this afternoon. So you can drop it off with the secretary of the art history department. She'll see that I get it. 
* Ok. Oh I almost forgot. I'm a biology major. But my advisor told me that I need one more humanities course to graduate. I've noticed that you are teaching a course on landscape painters next semester. Could tell me a little bit about it?
* Sure. Well, it's a course for non-art majors. We'll be looking at several different painters and examining their works. We'll be looking at several different painters and examining their works. We'll also look the history and politics of the era in which they lived.
* That sounds interesting. What else is required?
* There is no final exam. And there is only one required book. But each student has to give a major presentation about the individual painter at the end of the course.
* Hmm. It sounds good. Will you be in your office later today? I'd to talk to you some more.
* Well, my meeting's scheduled to last all afternoon. Why don't you stop by tomorrow? Any time in the afternoon. My office is in the fine arts building right next to the library.
* Thanks. I'll do that.

35. Why does the man call the woman?  (B)
36. What does the man almost forget to do?  (A)
37. What will the students be required to do in the course the woman describes?  (B)
38. What does the woman suggest the man do?  (C)

PART C
39 to 42 TALK IN AN ARCHITECTURE CLASS
As you probably know, log structures are gaining popularity. They are no longer just the simple country homes that we think of as the traditional log cabin. Some upscale homes now incorporate natural round logs in sealing beams and walls. People seem to think that the rounded logs give their homes a cozy warm atmosphere. And even people who want to build a traditional log cabin on their own can buy a kits with precut logs that fit together like pieces of lig-saw puzzle. Before showing you some slides of modern log houses, I'd like to give you a little historical background on the subject.
Log cabins were first built in the late 1600s along the Delaware river valley. The European immigrants who settled there brought centuries' old traditions of working with logs. And in this heavily wooded area logs were the material in hand. Log cabins were the most popular in the early 1800s with the settlers who were moving west. They provided the answer to the pioneer's need for a sale and sliding boards for windows. But the log buildings that have probably had most influence on modern architects are those of the mountain retreats of wealthy New Yorkers. These country houses which were popular in the early 1900s typify what's known as the Adoroundyx style. Now let's look at those slides.

39. What is the speaker mainly discussing?  (C)
40. According to the speaker, what gives modern log homes their warm atmosphere?  (C)
41. Why did the early settlers use log for building homes?  (B)
42. According to the speaker, why were log cabins especially popular with settlers who moved west?  (A)

43 to 46 TALK IN A GEOGRAPITY CLASS
The Old Canada Road is a long lost trail between the Canadian province of Quebec and Maine in the northeast corner of the United States. Yes it really was lost and finding it again was a complex process that involved state of our technology: how the location of the roads was pinpointed was very interesting. And I'll return to it as soon as I gave you a little background information. The road was begun in 1817, a few years before Maine even became a state. At the time Quebec was a major market for livestock, crops and fish. So a road to Quebec was seen by officials in Maine as necessary for trade. For about 20 years the movement of people and goods was mostly from Maine to Quebec, and then the trend reversed as thousands of Canadians immigrated to Maine to escape poor crops, the lack of jobs and the threat of disease. I think it was a color epidemic. Besides its negative reasons major building projects in Maine also made the state very attractive for the Canadians who needed work. I should stress though that immigration during that period went in both directions. In fact the flow of people and goods went completely unhindered. There wasn't even a border post until around 1850. The people of the time saw Maine and Quebec as single region mainly because of the strong French influence which is still evident in Maine today. Eventually the road fell into disuse as a major railway was completed. Finally people simply forgot about it and that's how it came to be lost. This brings me back to the original topic.

43. What does the speaker say about the road between Main and Quebec?  (B)
44. What is one reason Canadians began to immigrate to Maine during the 1800s?  (B)
45. What can be inferred about the region including Maine and Quebec during the early 1800s?  (A)
46. What subject is the speaker most likely to discuss next?  (D)

47 to 50 LECTURE IN A COLLEGE CLASS
OK, in the last class we talked about the classification of trees and we ended up with a basic description of angiosperm. You remember that those are plants with true flowers and seeds that develop into fruits. The common broad leaf trees we have on campus fall into this category. But our pines don't. Now I hope you all followed my advice and wore comfortable shoes because as I said today we are going to do a little field study. To get started let me describe a couple of broadleaf trees we have in front of us. I'm sure you've all noticed that this big tree next to Brett Hall. It's a black walnut that must be 80 feet tall. As a matter of fact there is a plaque identifying. It is the tallest black walnut in the state. And from here we can see the beautiful archway of trees at the commons. They are American elms. The ones along the commons were planted when the college was founded 120 years ago. They have distinctive dark green leaves that lock lopsided because the two sides of the leaf are unequal. I want you to notice the elm right outside the Jackson Hall. Some of the leaves have withered and turned yellow, maybe due to Dutch elm disease. Only a few branches seem affective so far but if this tree is sick it'll have to be cut down. Well, let's move on and I'll describe what we see as we go.
47. What are the students going to do during this class period?  (D)
48. In what class is this lecture probably being given?  (C)
49. What is remarkable about the black walnut tree outside Brett Hall?  (C)
50. What is the problem with the elm tree near Jackson Hall?  (B)
