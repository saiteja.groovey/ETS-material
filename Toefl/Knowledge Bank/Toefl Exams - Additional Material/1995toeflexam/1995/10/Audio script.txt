1995��10�� ��������

1. How is your paper coming along?
My typewriter is broken.
What does the man mean?

2. Have you tried Susan's apple-pie?
I got the last piece. And it was out of this world.
What does the woman mean?

3. If you're staying late, will you be sure to look up the office when you leave?
Oh. I hope I won't be more than an hour.
What does the man mean?

4. Nancy really wants to ski on Thursday.
Yes, but she can't. Can she?
What does the man say about Nancy?

5. Let me help you with those packages.
Thanks. But it is only 3 quarters of a block.
What does the woman tell the man?

6. Did you know that Arthur has three brothers living on three different continents?
He must get in incredible phone bill every month.
What does the woman imply about Arthur?

7. This calculator isn't working right.
I think you've got the battery in upside down.
What does the woman mean?

8. We should buy a good guide book and study it before our trip to Montreal.
We could. But they're so overpriced. What about the library?
What does the man imply?

9. I always want a little something to eat about this time of day.
So do i. Let's have a snack now and then have a light lunch later.
What are the speakers discussing?

10. I've just been over to my friend Tom's new apartment.
It's much bigger than my place.
But more expensive I bet.
What does the woman mean?

11. Could you mail these letters for me please?
   More letters? Your friends are going to be very happy to hear from you.
   What does the man imply about the woman?

12. Does Prof Ford always come to class?
Is ice cold?
   What does the man imply about Prof Ford?

13. Would you have some time this week to go over these questions with me?
How does tomorrow sound?
   What does the man mean?

14. Hey? John! John!
Save your breath. He's out of earshot.
What does the woman mean?

15. You only have water to serve your guests?
This isn't just water. This is imported mineral water.
What does the woman imply?

16. I see a new bookstore has just opened on Main street/
It may be a new store. But the books are far from new.
What does the man mean?

17. Alice has been spending a lot of time at the library lately.
Well. She's got a paper due and two final exams next week.
What has Alice probably been doing?

18. It's going to cost a fortune to get my car fixed.
Why don't you just trade it in for a new one.
What does the man suggest the woman do?

19. Winter is over at last. Time to pack up my gloves and boots.
I've been waiting for this for months.
What does the woman mean?

20. How did the game go the other night? Did your team with?
Are you kidding? That would be a first.
What does the man imply?

21. Danis told us he likes to play cards.
But we've invited him three times and he hasn't come once.
What can be inferred about Danis?

22. I invited my class home for coffee.
In this tiny place?
What does the man imply?

23. Excuse me, I don't understand why fight 213 has been delayed. The weather seems fine now.
I'm afraid New York got three times as much snow as we did here. It should be clear by morning though.
What does the woman imply?

24. Take a look at this gift catalog. Maybe we can find something to get Janet for her new house.
OK. But remember we can't afford a lot.
What does the woman mean?

25. Are you still waiting for Bob?
I don't know why I bother. The store will be closed by the time we got there.
What does the woman mean?

26. Would you like to come with us for coffee a little later?
I'm off caffeine medical restriction.
What does the woman mean?

27. Mary seems surprised that she got a research grant.
Well, she should have been, everybody knows that she's brilliant.
What does the man mean?

28. Mind if I leave my umbrella here in the hallway?
Not at all. But first shake it off outside, would you?
What does the woman tell the man?

29. According to this article, the former boxing champ is going to try again. He's coming out of the retirement for the third time.
Is a comeback at this age at all likely?
What does the man imply about the former box champion?

30. Don't you think Prof. Morrison's test was too difficult?
Well, I must admit I had been expecting more than just passing grade in biology.
What does the man mean?

PART B
31-34  
have you made any plans to go away during semester break? I've been thinking of skiing.
I really haven't had time to think about my vacation. I've been concentrating on getting ready for my exams, especially Philosophy. But I'll probably go to the beach.
Why the beach?
Well. It would be mice to get away from this cold weather and just lie ine the sun and relax after working so hard.
It's true that skiing does require work. And you have to get up early and wait in long lines for the chair lifts. Thanks. I think you help me make up my mind.
Sure. Now maybe you can use your mind to think about something else, like your studies.

31. With whom is the man speaking?
32. What has the woman mainly been thinking about?
33. Where will the woman probably spend her vacation time?
34. What does the woman think the man should do?

35-38  
Hello, Jim. I haven't seen you in a while.
What seem to be the problem?
Actually I'm a little embarrassed about coming here. I feel fine right now. but you know how much stuff is going around. Andy way every year around the holidays like clockwork I come down with something.
So you're interested in prevention. What symptoms do you usually get?
You know, cough, fever, runny nose, my head and bones ache, chills even. I'm usually miserable for a week and it ends up ruining my holidays.
Sounds like a typical flu to me. As you said, lots of people have it influence often strikes when people are over tried stressed out and not eating nutritious food. And also you increase your exposure to a virus when you're in big crowds where lots of people are coughing and sneezing.
I certainly spend a lot time in department stores around the holidays buying gifts for people.
Yes. And so you increase your exposure to airborne viruses just when your body's resistance is already low from all the running around you do.
So what can I do to ward off the flu?
Actually it's fairly simple. Get a lot of rest, eat well. That way your immune system will be boosted. And you'll be more able to fight off illness.
All these things make sense. But one more question. Aren't I bound to get sick anyway if there's an outbreak in the dorm?
Oh, you didn't mention you lived in the dormitory. In that case I'd also suggest you get immunized. The vaccine available prevents' the three main types of influenza. Why don't you go to the university health center. The shots are free there.
I'll do it right away. It will be nice to feel well during. The holidays for once.

35. Why did the man go to see his doctor?
36. How does the man describe his health problem? 
37. What might be a reason the man gets ill?
38. Why does the doctor suggest the man go to the university health center?

PART C  TALKS
39 to 42 A lecture in a architectural design course.
Dozens of valuable paintings and frescoes were badly damaged today when rain water poured through a roof at the state museum. As freak rainstorms lashed in the central and southern sections of the state, part of the museum's roof which was scheduled to be repaired collapsed and water cascaded into two storerooms. The museum director said that several well known pieces, including oils, watercolors and frescoes, had been damaged, although only one objects, a 19 century fresco was damaged beyond repair. She added that inspection for damages has not been completed and therefore she could not estimate costs of the repair and replacement.

39. What part of the museum flooded?
40. What was damaged?
41. What can not be restored?
42. What did the director say about the cost of damages?

43 to 46 
welcome to the New Bedford institute's, series of lectures on the mysteries of the sea. You may have noticed the drawing on the cover of the program for today's presentation. It depicts a sea creature that scientists have been interested in for many years. It is said that in the 19th century a group of fishermen were surprised by seeing a huge squid that they said was as big as a house and had enormous tentacles. The fishermen were frightened out of their wits according to reports from that time. The creature sank back into the ocean and was never seen again. Marine biologists believed that this species of giant squid called archituth is still exits. And is comes to represent how little we know about sea creatures. Compared with what we know about the animals on the land. We do know that there are many more different kinds of marine species than there are land species. But we just don't have the technology yet to do sustained research. Even short unmanned trips are so hard to accomplish. In fact researching in the deep oceans has been compared to flying an airplane overland throwing down a net and seeing what you get. In other words it's very hard to picture the whole situation when that's the way you have to get your samples. Most of what is known about deep sea creatures, which as I said is actually very little, comes from ocean beds that dried up long ago. We're fortunate to have a few samples of these fossil remains on display on the second floor. We'll take a look at them after a while. 

43. Why were the fishermen frightened of the squid?
44. What does the giant squid symbolize to biologists?
45. What does the airplane example illustrate about the deep sea research?
46. Where has most of the information about rare sea creatures been found?

47-50 
With the introduction of radio, newspaper publishers wondered how broadcasting would affect them. Many feared that radio as a quick and easy means of keeping people informed would displace the newspaper industry altogether.
Others hoped that the brief newscast heard on the air would stimulate listeners interested in the story so they'd buy the paper to get more information. This second idea turned out to be closer to the truth. Radio & print were not substitutes for each other but actually supported each other. You see the relation ship between different media is not always one of displacement but can be one of reinforcement. However this is not always the case. Take television & motion pictures for example, with the popularization of TV, the motion picture industry suffered greatly. Movie attendance dropped when audience members chose to stay at home and be entertained. Likewise. When a football game was shown on the air, the stands were often empty because fans chose to watch the game at home.

47. What is the main topic of this talk?
48. According to the speaker, what is the relationship between radio & the newspaper industry?
49. According to the speaker, how did the introduction of television affect motion pictures?
50. Why does the speaker mention a football game?


